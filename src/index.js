const JSCART_KEY = 'jscart';
class JSCart {
	constructor(options = {}){
		this.options = Object.assign({
			className: 'js-basic-cart',
			development: false,
			uid: null,
		}, options);
		// run
		console.log(this.options.className);
		this.initCart();
	}

	initCart(){
		// loadPersistentCart
		this.loadPersistentCart();
		// buildCart
		this.buildCart();
		// development purpose		
		if(this.options.development){
			this.setupDevElements();
		}
		// handle Events
		this.handleEvents();
	}

	buildCart(){
		const cartEle = document.getElementsByClassName(this.options.className);
		if(cartEle.length > 0){
			this.cart = cartEle[0];
		}
		let items = this.persistentCart.items;
		console.log(items);
	}

	loadPersistentCart(){
		this.persistentCart = {};
		const defaultCart = {
			uid: this.options.uid,
			items: []
		}
		
		if( localStorage.getItem(JSCART_KEY) != null){
			this.persistentCart = JSON.parse(localStorage.getItem(JSCART_KEY));
		}
		this.persistentCart = Object.assign({}, defaultCart, this.persistentCart);
		console.log(this.persistentCart);
		//JSON.stringify()
	}

	setupDevElements(){
		let bodyEle = document.getElementsByTagName('body')[0];
		let productForm = document.createElement('form');
		productForm.innerHTML += '<p><input data-pid="1" data-qty="1" type="button" value="Add To Cart" class="jscart-btn-update" /></p>';
		productForm.innerHTML += '<p><input data-pid="1" data-qty="1" type="button" value="Remove from Cart" class="jscart-btn-delete" /></p>';
		productForm.innerHTML += '<p>ID: <input type="text" name="pid" /></p>';				
		productForm.innerHTML += '<p>Qty: <input type="text" name="qty" /></p>';
		productForm.innerHTML += '<p><input type="submit" name="buy" value="Buy" /></p>';		
		bodyEle.prepend(productForm);
	}

	updateCartItem(id, qty, action){
		qty = parseInt(qty);		
		if(this.persistentCart.items.filter( item => item.id == id).length > 0){
			this.persistentCart.items = this.persistentCart.items
				.map( item => item.id == id ? {...item, qty: item.qty + qty} : {...item})
		}else{
			this.persistentCart.items.push({
				id: id,
				qty: qty
			})
		}
		console.log(this.persistentCart.items);
	}

	emptyCart(){

	}

	manipulateCartItems(items, action){
		if(items.length > 0){
			for(let i = 0; i< items.length; i++){
				items[i].onclick = (e) => {
					e.preventDefault();
					// console.log(e.target.getAttribute('data-pid'));
					this.updateCartItem(e.target.getAttribute('data-pid'), e.target.getAttribute('data-qty') , action);
				}
			}
		}
	}

	handleEvents(){
		let updateBtns = document.getElementsByClassName('jscart-btn-update');
		let deleteBtns = document.getElementsByClassName('jscart-btn-delete');
		this.manipulateCartItems(updateBtns, 'update');
		this.manipulateCartItems(deleteBtns , 'delete');
	}
}
module.exports = JSCart;